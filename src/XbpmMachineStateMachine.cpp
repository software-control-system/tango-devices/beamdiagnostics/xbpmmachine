static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/BeamDiag/XbpmMachine/src/XbpmMachineStateMachine.cpp,v 1.3 2008-08-26 13:44:44 nleclercq Exp $";
//+=============================================================================
//
// file :         XbpmMachineStateMachine.cpp
//
// description :  C++ source for the XbpmMachine and its alowed. 
//                method for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: nleclercq $
//
// $Revision: 1.3 $
//
// $Log: not supported by cvs2svn $
// Revision 1.2  2007/11/22 18:55:14  sebleport
// - auto gain other managed
//
// Revision 1.1  2007/11/20 16:17:45  stephle
// initial import
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <XbpmMachine.h>
#include <XbpmMachineClass.h>

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace XbpmMachine_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_current1_allowed
// 
// description : 	Read/Write allowed for current1 attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_current1_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_current2_allowed
// 
// description : 	Read/Write allowed for current2 attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_current2_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_current3_allowed
// 
// description : 	Read/Write allowed for current3 attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_current3_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_current4_allowed
// 
// description : 	Read/Write allowed for current4 attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_current4_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_verticalPosition1_allowed
// 
// description : 	Read/Write allowed for verticalPosition1 attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_verticalPosition1_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_verticalPosition2_allowed
// 
// description : 	Read/Write allowed for verticalPosition2 attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_verticalPosition2_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_gain_allowed
// 
// description : 	Read/Write allowed for gain attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_gain_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_enableAutomaticGain_allowed
// 
// description : 	Read/Write allowed for enableAutomaticGain attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_enableAutomaticGain_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_xPos_allowed
// 
// description : 	Read/Write allowed for xPos attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_xPos_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_zPos_allowed
// 
// description : 	Read/Write allowed for zPos attribute.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_zPos_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_Start_allowed
// 
// description : 	Execution allowed for Start command.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_Start_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmMachine::is_Stop_allowed
// 
// description : 	Execution allowed for Stop command.
//
//-----------------------------------------------------------------------------
bool XbpmMachine::is_Stop_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

}	// namespace XbpmMachine_ns
